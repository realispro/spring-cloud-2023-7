import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PublisherService} from '../../../shared/service/publisher.service';
import {Location} from '@angular/common';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-department-edit',
  templateUrl: './publisher-edit.component.html',
  styleUrls: ['./publisher-edit.component.css']
})
export class PublisherEditComponent implements OnInit {

  public editPublisherForm: FormGroup;
  submitted = false;
  private _$alive = new Subject();


  constructor(private _formBuilder: FormBuilder,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _publisherService: PublisherService,
    private _location: Location) {
  }

  ngOnInit(): void {
    this.createForm();
    this._activatedRoute.params.pipe(takeUntil(this._$alive)).subscribe(params =>{
      let id = params['id'];
      this._publisherService.fetchById(id).subscribe(result =>{
        this.editPublisherForm.get('id').setValue(id);
        this.editPublisherForm.get('name').setValue(result['name']);
        this.editPublisherForm.get('logo').setValue(result['logo']);
      });
    });

  }

  private createForm() {
    this.editPublisherForm = this._formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      logo: ['', Validators.required]
    });
  }

  get inputFieldValue() {
    return this.editPublisherForm.controls;
  }

  public submit() {
    this.submitted = true;
    if (this.editPublisherForm.invalid) {
      return;
    }
    this._publisherService.update(this.editPublisherForm.value).subscribe(result =>{
      this._router.navigate(['department']);
    });
  }

  cancel() {
    this.editPublisherForm.get('name').reset();
  }

  back() {
    this._location.back();
  }

  public ngOnDestroy(): void {
    this._$alive.next();
    this._$alive.complete();
  }

}
