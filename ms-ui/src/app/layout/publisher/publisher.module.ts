import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PublisherComponent} from './publisher/publisher.component';
import {PublisherRoutingModule} from './publisher-routing.module';
import {GridModule} from '@progress/kendo-angular-grid';
import {PublisherBindingDirective} from './directive/publisher-binding.directive';
import {PublisherViewComponent} from './publisher-view/publisher-view.component';
import {PublisherAddComponent} from './publisher-add/publisher-add.component';
import {ReactiveFormsModule} from '@angular/forms';
import {PublisherEditComponent} from './publisher-edit/publisher-edit.component';


@NgModule({
  declarations: [PublisherComponent, PublisherBindingDirective, PublisherViewComponent, PublisherAddComponent, PublisherEditComponent],
  imports: [
    CommonModule,
    PublisherRoutingModule,
    GridModule,
    ReactiveFormsModule
  ]
})
export class PublisherModule { }
