import {Component, OnInit} from '@angular/core';
import {PublisherService} from '../../../shared/service/publisher.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-department',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.css']
})
export class PublisherComponent implements OnInit {

  constructor(private _router: Router,
    private departmentService: PublisherService) { }

  ngOnInit(): void {
  }

  newBtnClick() {
    this._router.navigate(['publisher','new']);
  }

  delete(id:string){
    this.departmentService.delete(id).subscribe(result=>{
      this.departmentService.query({});
    });
  }


}
