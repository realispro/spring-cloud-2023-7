import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BookComponent} from './book/book.component';
import {BookViewComponent} from './book-view/book-view.component';
import {BookAddComponent} from './book-add/book-add.component';
import {BookEditComponent} from './book-edit/book-edit.component';

const routes:Routes=[
  {
    path:'',
    component:BookComponent
  },
  {
    path:'new',
    component:BookAddComponent
  },
  {
    path:':id/view',
    component:BookViewComponent
  },
  {
    path:':id/edit',
    component:BookEditComponent
  }
]
@NgModule({

  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookRoutingModule { }
