import {Component, OnInit} from '@angular/core';
import {Cart} from "../../../../shared/model/cart.model";
import {CartService} from "../../../../shared/service/cart.service";
import {interval, Subscription} from "rxjs";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  cart: Cart;
  subscription: Subscription;
  _cartService: CartService


  constructor(_cartService: CartService) {
    const source = interval(1000);
    const text = 'Your Text Here';
    this._cartService = _cartService;
    this.subscription = source.subscribe(val => this.refresh());
  }

  refresh(){
    //console.log("refreshing cart view...")
    this.cart = this._cartService.cart
    //console.log("cart items:" + this.cart.items);
  }

  removeItem(id:number){
    this._cartService.remove(id);
  }

  confirm(){
    this._cartService.confirm();
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
