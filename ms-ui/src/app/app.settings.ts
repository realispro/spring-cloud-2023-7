export class AppSettings {

    public static APP_NAME = 'Librarian';

    public static SERVER_URL = 'http://localhost:5555';

    public static API_ENDPOINT = '';

    public static LOGIN_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + '/service-auth/oauth/token';

    public static USER_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + '/service-auth/user';

    public static ACCESS_TOKEN = 'access_token';

    public static CURRENT_USER = 'current_user';

    public static PUBLISHER_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + "/ms-publisher/publishers";

    public static BOOK_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + "/ms-book/books";

    public static CART_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + "/ms-cart/carts";

    public static AUTHORIZATION_HEADER_NAME = 'Authorization';
}
